export const posts = [{
  title: 'Angular Package Format 简介',
  slug: 'introduce-angular-package-format',
  tags: ['angular'],
  date: '2018-03-15T15:21:28.264Z',
}, {
  title: '2017 HTTPS 调查',
  slug: 'https-in-2017',
  tags: ['https'],
  date: '2017-12-07T07:42:27.752Z',
}, {
  title: 'NgModule 的作用域',
  slug: 'ngmodule-and-its-scope',
  tags: ['angular'],
  date: '2017-10-16T06:10:47.147Z',
}, {
  title: 'Spacemacs 和 Org-mode 和 LaTeX',
  slug: 'spacemacs-plus-org-mode-plus-latex',
  tags: ['emacs', 'latex', 'org-mode'],
  date: '2017-06-27T07:32:10.831Z',
}, {
  title: '基于 React 的 SEO 友好的博客',
  slug: 'make-a-react-based-blog-seo-friendly',
  tags: ['blog', 'react', 'seo'],
  date: '2017-06-15T13:35:03.930Z',
}, {
  title: 'Solomon 现已支持 AMP',
  slug: 'solomon-now-supports-amp',
  tags: ['amp', 'blog'],
  date: '2017-03-26T11:54:06.296Z',
}, {
  title: '同步你的 JetBrains 设置',
  slug: 'sync-your-jetbrains-settings',
  tags: ['github', 'jetbrains'],
  date: '2017-03-18T05:36:11.165Z',
}, {
  title: '入手 PS4 Slim',
  slug: 'bought-ps4-slim',
  tags: ['game', 'ps4', 'sony'],
  date: '2017-03-01T05:57:14.898Z',
}, {
  title: '通过 Firebase Authentication 进行 OAuth 授权',
  slug: 'oauth-via-firebase-authentication',
  tags: ['firebase', 'github', 'oauth'],
  date: '2017-02-19T15:36:31.925Z',
}, {
  title: '部署博客到 GitHub Pages & Firebase',
  slug: 'deploy-blog-via-gh-pages-and-firebase',
  tags: ['angular', 'firebase', 'github'],
  date: '2017-02-05T13:57:17.024Z',
}, {
  title: '新的博客系统 Solomon',
  slug: 'introducing-solomon',
  tags: ['angular', 'blog'],
  date: '2017-01-15T16:04:11.888Z',
}, {
  title: 'Hello World!',
  slug: 'hello-world',
  tags: ['thought'],
  date: '2017-01-05T08:30:55.961Z',
}];
