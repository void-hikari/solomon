import { Component } from '@angular/core';

@Component({
  selector: 'solomon-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {}
