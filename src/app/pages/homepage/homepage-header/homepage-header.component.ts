import { Component } from '@angular/core';

@Component({
  selector: 'solomon-homepage-header',
  templateUrl: './homepage-header.component.html',
  styleUrls: ['./homepage-header.component.scss'],
})
export class HomepageHeaderComponent {}
