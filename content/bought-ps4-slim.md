> 2017-03-14更新: 新增了 刺客信条: 大革命 和 杀戮地带: 暗影坠落
> 的游戏体验部分.

Why PlayStation?
---

为什么选择 Sony PlayStation?
而不是 XBox One?
甚至是即将出的 Nintento Switch?

主要是因为 PlayStation 上的独占游戏相比其他两个主机平台更多更全.

> 例如: [尼尔: 机械纪元](https://en.wikipedia.org/wiki/Nier:_Automata)(~~2B小姐姐~~)

此外 PS4 的性能相比 XBox One 也更加强.
当然, XBox 也在奋起直追,
推出了 **天蝎座(Project Scorpio)**.
不过鉴于目前天蝎座还为上市,
XBox 能否借此翻身还是未知数.

Why Slim?
---

PS4 目前简略的来说有三个大版本:
PS4(即初版), PS4 Slim, PS4 Pro.

![](https://c1.staticflickr.com/3/2913/33297552255_446f3a52fe_o.jpg)

_三版PS4的对比, 从上到下分别是 PS4 Slim, PS4 和 PS4 Pro_

原版的 PS4 和 PS4 Slim 已经没有任何的可比性了:

1. PS4 Slim 机身比 PS4 更加 **轻薄**, 且 **散热** 更好, **噪音** 更少.

2. PS4 Slim 支持 **5G** 频段 WiFi.

3. PS4 Slim 的性能和 PS4 **相同**, 且价格更 **便宜**.

4. PS4 Slim 使用了 **新版的 DualShock 4 手柄**.

而相比之下,
PS4 Pro 的提升不多,
主要都是画质的提升:

1. 输出 **4K 画面**

2. 支持 **HDR**

3. 运行游戏更加流畅

而且部分游戏还对 PS4 Pro 进行了优化,
可以获得更强的游戏性能.
这部分游戏会标注为 **ENHANCED 强化游戏**:

![ps4-pro-enhanced](https://c1.staticflickr.com/3/2848/33303850465_b012a05d4f_o.jpg)

_强化游戏的LOGO_


画面什么的目前我不太在意,
因为我本身的屏幕也不是什么好屏幕,
只是一个普通的 1080P 而已.

所以最后综合我目前的情况,
我选择了 PS4 Slim.

此外因为我的 PSN 帐号是港服的,
而国行的主机登录不了国服以外的帐号,
所以我买了港版的主机.

上手体验
---

拿到主机之后,
第一眼就看出 Slim 通体用的是磨砂的塑料,
而不是 PS4 的那种亮面的塑料,
显得更加的低调.

![sony-ps4-logo-closeup](https://c2.staticflickr.com/4/3885/33297553145_834dc9f216_b.jpg)

_SONY LOGO PS4 LOGO 特写_

可以看出,
PS4 Slim 并没有采用 PS4 上那个有点花哨的个感应式开机键,
而是换成了普通按键.

> 实际上 SONY 在 PS4 12xx系里就已经意识到静电开关的问题了,
> 将开关改成了普通的按键.
>
> 不过同时也去掉了 10xx 和 11xx 的漂亮的烤漆表面... sad

![dualshock4](https://c1.staticflickr.com/1/760/33297552625_b8b83d8f89_b.jpg)

_DUALSHOCK4手柄_

新版的 DUALSHOCK4 手柄,
相比旧款的,
改进的地方不多.
主要是改进了LED灯,
可以通过触摸屏上看到LED灯的状态;
还有就是按键改成磨砂质感了.

![dulashock4-vs-xbox-one-controller](https://c1.staticflickr.com/3/2896/33169374751_e2d35245d0_b.jpg)

_DUALSHOCK 4 vs XBox One Controller_

> XBox One 的手柄是 **泰坦陨落(Titanfall)** 版

游戏体验
---

![horizon-zero-dawn](https://c1.staticflickr.com/1/694/33169375011_61e98fb5f7_o.jpg)

_地平线: 黎明时分(Horizon: Zero Dawn)_

地平线是我和 PS4 Slim 一起入的,
于 2月28日 发售.

经过了几天的游玩之后,
我感觉 PS4 Slim 驾驭 1080p@30fps 还是绰绰有余的.

游戏全程都很流畅,
一些部分场景会发生BGM播放卡顿的问题,
应该是游戏优化的问题.

此外, 我还趁 PSN 港服优惠,
入手了 **刺客信条: 大革命**(Assassin's Creed Unity)
和 **杀戮地带: 暗影坠落**(Killzone Shadow Fall).

> 值得一提的是,
> 杀戮地带和地平线是都是由 Sony 第一方游戏公司
> Guerrilla 开发的.
>
> 此外, 他们还开发了
> RIGS: 机械化战斗联盟(RIGS Mechanized Combat League).

其中, 杀戮地带的运行十分流畅;
而刺客信条则不仅偶有卡顿, 而且不支持 PS4 的 Reset 模式,
每次进入待机模式回来之后都需要重新打开游戏.
所以只能归咎于育碧的渣优化了,
毕竟当初育碧就由于大革命的游戏 Bug 太多而将 DLC 免费赠送.

后语
---

**#archlinux-cn PSN 分部** 已成立:

![archlinux-cn-psn](https://c1.staticflickr.com/1/672/32921094540_d71432a809_o.jpg)

欢迎大家入驻 :)
